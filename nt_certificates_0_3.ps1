<#
    .SYNOPSIS
         Überprüft die Ablaufdaten von Zertifikaten
    .DESCRIPTION
         Das Skript liest alle Zertifikate des Systems aus. Anschließend wird das Ablaufdatum jedes Zertifikats überprüft. Es wird alarmiert, sobald ein Zertifikat in weniger als den angegebenen Tagen  abläuft.
    .PARAMETER Directory
         Pfad zum Verzeichnis, in dem die Zertifikate liegen
    .PARAMETER Warning
         Schwellwert für Warning-Event
    .PARAMETER Critical
         Schwellwert für Critical-Event
    .PARAMETER ThresholdExpection
         Tage, die Zertifikate abgelaufen sein müssen, damit sie nicht alarmieren
	.PARAMETER IncludeOk
		 Gibt an, ob auch Zertifikate ohne zeitnahes Ablaufdatum aufgeführt werden sollen
    .EXAMPLE
         .\nt_certificates_0_3.ps1 -Directory "Cert:\LocalMachine"  -Warning 14 -Critical 0
    .SHORTMESSAGE
         CRITICAL - 2 certificates below critical threshold.
    .LONGMESSAGE
         MonitoringStatus  ExpiresOn      DaysUntilExpired       Subject
		 ---------------   ---------      ----------------       --------
		 CRITICAL          Ablaufdatum    Tage bis abgelaufen    Hostname
    .PERFDATA
        none
    .REQUIREMENTS
		
     .TIMEOUT
		Default
#>

#region Parameter
Param (
    [string]$Directory = "Cert:\LocalMachine\My",
	[switch]$Recursive = $false,
    [int]$Warning = 14,
    [int]$Critical = 0,
	[int]$ThresholdExpection = -30,
    [switch]$IncludeOk = $false
)
write-verbose("Region: Parameter")
#endregion

#region Variablen
write-verbose("Region: Variablen")
$Error.Clear()
$ErrorActionPreference = "Continue"
$Severity=0
$ShortMessage=""
$LongMessage=""
$Results = $Null
$Warnings = @()
$Criticals = @()
$Ok = @()
#endregion

#region Zertifikate abfragen
write-verbose("Region: Zertifikate abfragen")
if($Recursive -eq $true) {
	$Certificates = Get-ChildItem $Directory -Recurse | Select-Object NotAfter,Subject,Issuer,@{Name = 'Threshold'; Expression = { ($_.NotAfter - (Get-Date)).Days }}
	$CertificatesUnique = $Certificates | Sort-Object Subject,Threshold -Descending | Group-Object -Property Subject | ForEach-Object { $_ | Select-Object -ExpandProperty Group | Select-Object -First 1 }
}
else {
	$Certificates = Get-ChildItem $Directory | Select-Object NotAfter,Subject,Issuer,@{Name = 'Threshold'; Expression = { ($_.NotAfter - (Get-Date)).Days }}
	$CertificatesUnique = $Certificates | Sort-Object Subject,Threshold -Descending | Group-Object -Property Subject | ForEach-Object { $_ | Select-Object -ExpandProperty Group | Select-Object -First 1 }
}
#endregion

#region Zertifikate auf Ablaufdatum überprüfen
write-verbose("Zertifikate auf Ablaufdatum überprüfen")
if($CertificatesUnique) {
	foreach($Cert in $CertificatesUnique) {
        if($Cert.Threshold -le $Critical -and $Cert.Threshold -gt $ThresholdExpection) {
            $Criticals += New-Object psobject -Property @{
				Issuer = $Cert.Issuer
                DaysUntilExpired = $Cert.Threshold
                Subject = $Cert.Subject
				ExpiresOn = $Cert.NotAfter
                MonitoringStatus = "CRITICAL"
			}
        }
		elseif($Cert.Threshold -le $Warning -and $Cert.Threshold -gt $ThresholdExpection) {
			$Warnings += New-Object psobject -Property @{
				Issuer = $Cert.Issuer
                DaysUntilExpired = $Cert.Threshold
                Subject = $Cert.Subject
				ExpiresOn = $Cert.NotAfter
                MonitoringStatus = "WARNING"
			}
		}
        else {
            $Ok += New-Object psobject -Property @{
				Issuer = $Cert.Issuer
                DaysUntilExpired = $Cert.Threshold
                Subject = $Cert.Subject
				ExpiresOn = $Cert.NotAfter
                MonitoringStatus = "OK"
			}
        }
	}
}
#endregion

#region Erzeugung der Ausgabe
write-verbose("Region: Erzeugung der Ausgabe")
$LongMessage += "The following status was determined: "
if($Criticals.Count -ne 0 -or $Warnings.Count -ne 0 -or $Ok.Count -ne 0) {
    if($IncludeOk -eq $true) {
        $LongMessage += $Criticals + $Warnings + $Ok | sort ExpiresOn | select MonitoringStatus, ExpiresOn, DaysUntilExpired, Subject | ft -AutoSize -Wrap | Out-String -Width 140
    }
    else {
        $LongMessage += $Criticals + $Warnings | sort ExpiresOn | select MonitoringStatus, ExpiresOn, DaysUntilExpired, Subject | ft -AutoSize -Wrap | Out-String -Width 140
    }
}
else {
    $LongMessage += "No Certificates found."
}

if($Warning -eq -1 -and $Critical -eq -1) {
	$ShortMessage += "UNKNOWN - No thresholds given."
    $Severity = 3
}
elseif($Warnings.Count -eq 0 -and $Criticals.Count -eq 0) {
	$LongMessage = ""
	$ShortMessage += "OK - All server certificates in desired state."
    $Severity = 0
}
elseif($Criticals.Count -eq 0) {
    $ShortMessage += "WARNING - $($Warnings.Count) certificates below warning threshold."
	$Severity = 1
}
elseif($Warnings.Count -eq 0) {
	$ShortMessage += "CRITICAL - $($Criticals.Count) certificates below critical threshold."
	$Severity = 2
}
else {
	$ShortMessage += "CRITICAL - $($Criticals.Count) certificates below critical threshold. $($Warnings.Count) certificates below warning threshold."
	$Severity = 2
}
#endregion

#region Ausgabe
write-verbose("Region: Ausgabe")
Write-Output $ShortMessage
if($LongMessage) {
    Write-Output $LongMessage
}
#endregion

# if debugging is active parse logfile to local shell
if((Test-path $g_log) -and ($PSBoundParameters['Verbose'])) {
    get-content $g_log
}

Exit $Severity